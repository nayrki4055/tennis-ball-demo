# Tennis Ball Demo

Demo code for Volley  

This application tracks the movement of tennis balls across a designated target.  

It identifies the regions in the target precisely using the center of the lines, and then labels the region the ball is currently in.  



## Configuration

This Python3 application has OpenCV and numpy as dependencies. It was developed with python 3.6.8 but should work with any version of python3.  

The configuration of this application is to use AVI output with the MJPG codec, which should be OS-portable.  


## Usage
```
python ball_tracking.py input_filename [--no-trace] [--live] [-o output_filename]

--no-trace disables the positional tracking of balls
--live shows a live view in an OpenCV window

```
