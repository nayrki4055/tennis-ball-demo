import cv2
import numpy as np

## stub in case a different version of opencv is installed that has a 3-part return
def findContours(img,retr_type,optimize):
    try:
        cnt,tree = cv2.findContours(img,retr_type,optimize)
    except:
        x,cnt,tree = cv2.findContours(img,retr_type,optimize)
    return cnt,tree

#gets raw line samples representing the top and bottom edges of the top and bottom lines
#returns 0 values for anything invalid
#input: mask of line pixels
#output: boundary lines d1,d2,d3,d4 are points where the mask flips values
def getLineOutlines(mask):
    height, width =mask.shape[:2]
    #find transition points in the mask
    delta = mask[1:]-mask[:-1]
    
    #find the initial line values
    #When using argmin/argmax on a labeled array,
    #this function returns the first value reaching the min/max
    
    #Find the starts/stops of the lines     
    # along the y-axis for each x-coordinate
    d1 = np.argmax(delta,axis=0) # rising edge of top line
    d2 = np.argmin(delta,axis=0) # falling edge of top line
    
    #invert the search direction on the delta array to find the bottom lines
    d3 = np.argmax(delta[::-1],axis=0)  # rising edge of bottom line
    d4 = np.argmin(delta[::-1],axis=0)  # falling edge of bottom line
    
    #cleanup any misdetected lines
    d2mid = height -d2.mean()
    d3mid = height - d3.mean()
    d1[d2>d3mid]=0  # if a point in the top line is inside the range of the bottom line, it's clearly invalid
    d2[d2>d3mid]=0
    d4[d3>d2mid]=0  # vice versa
    d3[d3>d2mid]=0
    
    #return img,None
    #handle edge cases with missing rising or trailing edge
    d1[d1>d2] = 0
    d4[d4>d3] = 0
    #identify offset from d2/d3 for first and last full segments, and use that size
    rising = np.argmax(d1>0)
    falling = width-np.argmax(d1[::-1]>0)-1
    
    d1[:rising] = d2[:rising] - (d2[rising]-d1[rising])
    d1[falling:] = d2[falling:] - (d2[falling]-d1[falling])
    
    rising = np.argmax(d4>0)
    falling = width-np.argmax(d4[::-1]>0)-1
    
    d4[:rising] = d3[:rising] - (d3[rising]-d4[rising])
    d4[falling:] = d3[falling:] - (d3[falling]-d4[falling])
    
    
    #put bottom lines in same coordinate space
    d3= height - d3 -2   #-2 because of 1px detection shift
    d4= height - d4 -2  
    
    return d1,d2,d3,d4
    
# line intersection formula for lines defined as mx+b for two sets of points
def find_center_point(a1,a2,b1,b2):
    ma = (a2[1]-a1[1])/(a2[0]-a1[0])
    mb = (b2[1]-b1[1])/(b2[0]-b1[0])
    
    ba = a1[1] - ma*a1[0]
    bb = b1[1] - mb*b1[0]

    x = (bb-ba)/(ma-mb)
    y = ma*x+ba
    return (int(x),int(y))


# draws the line centers, center point, and highlights 
# the appropriate sector if a ball is present
def display_field(img,line_data,ball_data):
    line1 = line_data[0]
    line2 = line_data[1]
    stride = line_data[3]
    center_x = line_data[2][1]
    
    display_l1 = np.clip(line1,0,img.shape[0]-1)
    display_l2 = np.clip(line2,0,img.shape[0]-1)

    imgout = img.copy()

    ls1_color = (255,0,0)
    ls2_color = (255,0,0)
    ls3_color = (255,0,0)
    ls4_color = (255,0,0)

    if ball_data is not None:
        zone = check_point(*ball_data[0],line_data)
        if zone == 1:
            ls1_color = (0,255,255)
            ls4_color = (0,255,255)
        if zone == 2:
            ls1_color = (0,255,255)
            ls2_color = (0,255,255)
        if zone == 3:
            ls2_color = (0,255,255)
            ls3_color = (0,255,255)
        if zone == 4:
            ls3_color = (0,255,255)
            ls4_color = (0,255,255)



    if stride == 1:
        imgout[display_l1,np.arange(img.shape[1])]=(255,0,0)
        imgout[display_l2,np.arange(img.shape[1])]=(255,0,0)
    else:
        for x in range(len(line1)-1):
            if x<center_x:
                color1 = ls1_color
                color2 = ls4_color
            else:
                color1 = ls3_color
                color2 = ls2_color
            cv2.line(imgout,(x*stride,line1[x]),((x+1)*stride,line1[x+1]),color1,3)
            cv2.line(imgout,(x*stride,line2[x]),((x+1)*stride,line2[x+1]),color2,3)
            
            if x == center_x:
                a1 = ((x-1)*stride,line1[x-1])
                a2 = ((x*stride),line1[x])
                b1 = ((x-1)*stride,line2[x-1])
                b2 = ((x*stride),line2[x])
                real_center = find_center_point(a1,a2,b1,b2)
                
                cv2.line(imgout,b1,real_center,ls4_color,3)
                cv2.line(imgout,real_center,b2,ls2_color,3)
                cv2.line(imgout,a1,real_center,ls1_color,3)
                cv2.line(imgout,real_center,a2,ls3_color,3)
                cv2.circle(imgout,real_center,10,0,-1)
            
    return imgout

#This function segments the field based on the lines
#Originally, I had this defining the lines based on a 
#2-point method and inferring any lines missing due
#to occlusion from the ball. However, the physical lines 
#are curved / not uniform, so I rewrote it to define the 
#lines on a strided basis so that the edge is always
#accurate
#Additionally, this function has a persistence metric
#so that if a ball has a very high whiteness value
#or rolls across a line, the missing values are re-used
#from the previous time they were seen

#striding > 1px is used to maintain real time performance
#with this unoptimized code as the argmax functions are
#not optimized well for this function

#input: 
#Raw image 
#previous line points
#optional, ball data to annotate region
#optional, threshold for detecting errors
#outputs: 
#annotated image (optional)
#
#metadata consisting of (line1,line2,(center region data),stride)

def segment_field(img,prev_data=None,ball_data=None, display = ["lines","zones"],stride=30,camera_thresh=5):
    #mask based on the coloration of the image
    
    sample_img = img[:,::stride]
    sample_height,sample_width = sample_img.shape[:2]
    
    
    
    mask = np.int8((get_white(sample_img)>0.85))
    
    # get line transitions
    d1,d2,d3,d4 = getLineOutlines(mask)
    
    # define initial lines as the halfway point between the transitions
    top_line = np.int32((d1+d2)/2)
    bot_line = np.int32((d3+d4)/2)

    # handle center case to make lines continuous through the intersection
    cross_found = False
    xs =0  ## starting x
    xe =0  ## ending x
    
    ##estimated y-axis center for each point along both inner lines. This converges to the actual center
    center_y_estimate = (d3+d2)/2     
    
    # when the center y value is reached, d3 == d1 and d2==d4 
    # however, because of striding, we're likely going to miss
    # that window so just pick a larger window around the center
    for rollback in range(10,300,5):
        
        xs1 = np.argmax(d2 > (center_y_estimate - rollback))-1
        xe1 = sample_width-1-np.argmax(d2[::-1] > center_y_estimate - rollback)+1
        xs2 = np.argmax(d3 < (center_y_estimate + rollback))-1
        xe2 = sample_width-1-np.argmax(d3[::-1] < center_y_estimate + rollback)+1
        xs =min(xs1,xs2)
        xe = max(xe1,xe2)

        if xs > 0 and xs2 > 0 and xe < (sample_width-1) and xe2 < (sample_width-1):
            cross_found = True
            break                
            
    # convert the lines from top/bottom to unified diagonal
    # and interpolate the coordinates in the crossing zone
    
    if cross_found:
        #handle line1
        line1 = np.empty(sample_width,dtype=np.int32)
        
        ys = top_line[xs-1]
        ye = bot_line[xe]
        length = xe-xs + 1
        
        line1[:xs]=top_line[:xs]
        line1[xe:]=bot_line[xe:]
        line1[xs:xe] = np.arange(1,length) * ((ye-ys)/length) + ys
        
        #handle line2
        line2 = np.empty(sample_width,dtype=np.int32)
        
        ys = bot_line[xs-1]
        ye = top_line[xe]
        length = xe-xs + 1
        
        line2[:xs]=bot_line[:xs]
        line2[xe:]=top_line[xe:]
        line2[xs:xe] = np.arange(1,length) * ((ye-ys)/length) + ys
        
        center_x = np.argmax(line1>=line2)
    else:   
# since the central region was unable to be identified, use values from the previous frame
        
        if prev_data is not None:
            ## set it up, let the continuity manager fix it
            center_x = prev_data[2][1]
            line1 = np.empty(sample_width,dtype=np.int32)
            line2 = np.empty(sample_width,dtype=np.int32)
            xs = prev_data[2][0]
            xe = prev_data[2][2]
            line1[:xs] = top_line[:xs]
            line1[xe:] = bot_line[xe:]
            line2[:xs] = bot_line[:xs]
            line2[xe:] = top_line[xe:]
        else:
            return None
    
    ## add an extra point at the end so that the last few pixels in the frame are covered
    if stride != 1:
        line1 = np.append(line1,line1[-1]+(line1[-1]-line1[-2]))
        line2 = np.append(line2,line2[-1]+(line2[-1]-line2[-2]))
    
    ## Compare lines to previously detected values to ID and fix any broken regions
    # allow some movement due to camera 
    if prev_data is not None:
        dl1 = line1 - prev_data[0]
        dl2 = line2 - prev_data[1]
        
        line1_continuity =  np.abs(dl1) < camera_thresh 
        line2_continuity =  np.abs(dl2) < camera_thresh 
        
        line1[~line1_continuity] = prev_data[0][~line1_continuity] 
        line2[~line2_continuity] = prev_data[1][~line2_continuity] 
        
    
    #prepare output data
    line_data = (line1,line2,(xs,center_x,xe),stride)

    
    imgout = display_field(img,line_data,ball_data)
    
    return imgout,line_data



## figures out which zone the ball is in
def check_point(x,y,lines):
    if lines[3] == 1:
        l1 = ( y> lines[0][x] )
        l2 = ( y> lines[1][x] )
    else:
        segment = int(x/lines[3])
        offset = x%lines[3]
        offset_ratio = float(offset)/lines[3]
        predict1 = lines[0][segment]*(1-offset_ratio) + lines[0][segment+1] * offset_ratio
        predict2 = lines[1][segment]*(1-offset_ratio) + lines[1][segment+1] * offset_ratio
        l1 = y > predict1
        l2 = y > predict2
    
    ### define zones by the lines
    if l1 and not l2:
        return 1
    if not l1 and not l2:
        return 2
    if not l1 and l2:
        return 3
    if l1 and l2:
        return 4

    
# finds a ball within a masked image
def findBall(mask,ball_history=[]): 
    cnt,tree=  findContours(mask,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    cnt = [cont for cont in cnt if cv2.contourArea(cont)>1000]
    if len(cnt)>0:
    
        ball_contour = max(cnt,key=lambda x:cv2.contourArea(x))
        try:
            x,y,w,h = cv2.boundingRect(ball_contour)
        except:
            print(ball_contour)
        
        ball_pos = (int(x+w/2),int(y+h/2))
        ball_topleft = (x,y)
        ball_botright = (x+w,y+h)
        ball_data = (ball_pos,ball_topleft,ball_botright)
        
        ball_history.append(ball_pos)
                
        return ball_contour,ball_data,ball_history
    else:
        return None,None,[]

    
#defines colors by the ratios of their components
def get_cratio(img):
    img = np.float32(img)
    return img/np.maximum(1,img.sum(axis=-1)[...,None])
    #b,g,r,t=get_comps(img)
    #return b/t,g/t,r/t

#quantifies how white a color is
def get_white(img):
    img_ratio = get_cratio(img)
    return 1.0 - np.abs(1/3.0 - img_ratio).sum(axis=-1)

def mask_ball(img,bgratio_prev=None,delta_thresh=0.17,erode=5):
    g = img[...,1]
    b = np.maximum(1,img[...,0])
    bgratio = (g/b)
    if bgratio_prev is not None:
        delta = (bgratio - bgratio_prev)
        mask = cv2.erode(np.uint8(delta>delta_thresh),np.ones((erode,erode)))
    else:
        mask = np.zeros(img.shape[:2],dtype=np.uint8)
    return mask,bgratio
    
class BallTracker:
    def __init__(self,showTrace=True,liveView=False):
        self.prev_balls = []
        self.ball_history = []
        self.prev_line_data=None
        self.prev_bgratio=None
        self.showTrace=showTrace
        self.liveView=liveView
    
    def track(self,img):
        
        ball_mask,self.prev_bgratio = mask_ball(img,self.prev_bgratio)
        ball_contour,ball_data,cur_ball_history = findBall(ball_mask,ball_history=self.ball_history)
        
        imgout,self.prev_line_data = segment_field(img,prev_data=self.prev_line_data,ball_data=ball_data)
        
        if ball_contour is None and len(self.ball_history)>0:
            self.prev_balls.append(self.ball_history)
            self.ball_history = []
        if ball_contour is not None:
            self.ball_history = cur_ball_history
            ball_pos,ball_topleft,ball_botright = ball_data
            cv2.drawContours(imgout,[ball_contour],-1,0,2)
            cv2.rectangle(imgout,ball_topleft,ball_botright,(0,0,255),2)
            prev_pos = None
            if self.showTrace:
                for pos in self.ball_history:
                    cv2.circle(imgout,pos,4,(0,255,0),-1)
                    if prev_pos is not None:
                        cv2.line(imgout,pos,prev_pos,(255,0,0),2)
                    prev_pos=pos
        
        if self.liveView:
            cv2.imshow("test",imgout)
            cv2.waitKey(1)
        
        return imgout,ball_data
    
    def getHistory(self):
        return self.prev_balls