from BallTracker import BallTracker
import cv2
from sys import argv,stdout


if __name__ == "__main__":
    if len(argv)>1:
        filename = argv[1]
    else:
        print("No file specified")
        print("Usage:")
        print(" python ball_tracking.py [input filename] [--live] [--no-trace] [-o output filename]")
        print(" --live: Show a live view in an output window")
        print(" --no-trace: Disable path tracing")
        exit(0)
    
    tracker = BallTracker()
    
    if "--live" in argv:
        tracker.liveView=True

    if "--no-trace" in argv:
        tracker.showTrace=False

    output_filename = filename[:-3]+"output.avi"
    
    for x,arg in enumerate(argv):
        if x+2<len(argv) and arg == "-o":
            output_filename = argv[x+1]
            if output_filename[-4:]!=".avi":
                output_filename = output_filename +".avi"
    
    vc = cv2.VideoCapture(filename)
    vw = None
    frame_no=0
    while True:
        ret,img =vc.read()
        if not ret:
            break
        if vw is None:
            vw = cv2.VideoWriter(output_filename,cv2.VideoWriter_fourcc(*"MJPG"),30,(img.shape[1],img.shape[0]),True)
        imgout,ball = tracker.track(img)
        vw.write(imgout)
        stdout.write("\rProcessed frame %d"%frame_no)
        stdout.flush()
        frame_no=frame_no+1
        
    vw.release()
    print("\nCompleted")
    